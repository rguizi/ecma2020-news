// Possível importar um módulo dinamicamente utilizando async/await
// deve ser utilizada nova sintaxe do ecmaScript no export/import

const doMath = async (num1, num2) => {
  if (num1 && num2) {
    const math = await import('./math.js')
    console.log(math.add(5, 10))
  }
}

doMath(4, 2)