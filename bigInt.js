// Manipular número maiores do que o MAX_SAFE_INTEGER
// acrescentar o 'n' no final do número
// todo número que for interagir com um bigInt deve possuir o n no final

// Sem utilizar o BigInt
const maxInteger = Number.MAX_SAFE_INTEGER;
console.log(maxInteger) // 9007199254740991
console.log(maxInteger + 1) // 9007199254741992
console.log(maxInteger + 2) // 9007199254740992 ???

// Utilizando o BigInt
const maxIntegerBigInt = BigInt(maxInteger)
console.log(maxIntegerBigInt) // 9007199254740991n
console.log(maxIntegerBigInt + 1n) // 9007199254740992n
console.log(maxIntegerBigInt + 2n) // 9007199254740993n
console.log(maxIntegerBigInt + 100n) // 9007199254741091n
console.log(typeof(maxIntegerBigInt)) // bigint

console.log(maxIntegerBigInt/2n)
