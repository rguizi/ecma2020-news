// Retorna com o resultado de todas as promises, mesmo que uma ou mais tenham rejeitado

const p1 = new Promise((res, rej) => setTimeout(res, 1000))
const p2 = new Promise((res, rej) => setTimeout(rej, 1000))
const p3 = new Promise((res, rej) => setTimeout(rej, 1000))

Promise.allSettled([p1, p2, p3]).then(data => console.log(data))
/*
[
  { status: 'fulfilled', value: undefined },
  { status: 'rejected', reason: undefined },
  { status: 'rejected', reason: undefined }
]
*/