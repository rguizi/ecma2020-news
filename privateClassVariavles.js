class Message {
  #message = "Hello World"
  greet() { console.log(this.#message) }
}

const greeting = new Message()

greeting.greet() // Howdy
console.log(greeting.#message) // Private field '#message' must be declared in an enclosing class