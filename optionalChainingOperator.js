// Facilita a verificação de parâmetros de objeto
// já utilizando o operador nullish coalescing

const person = {}

console.log(person.profile.name ?? 'Nome não informado') // Cannot read property 'name' of undefined

if (!person || !person.profile || !person.profile.name) {
  console.log('Nome não informado') // Nome não informado
}

// Optional Chaining Operator
console.info(person?.profile?.name ?? 'Nome não informado') // Nome não informado