// Verifica o conteudo de uma maneira mais restrita, retorna o valor default (lado direito) somente quando o valor da esquerda for null ou undefined

const person = { profile: { name: '', age: 0 }}

// Com operator OR
console.log(person.profile.name || "Anonymous") // Anonymous
console.log(person.profile.age || 18) // 18

// Com operator Nullish
console.log(person.profile.name ?? "Anonymous") // ""
console.log(person.profile.age ?? 18) // 0