//String.prototype.matchAll

// retorna array com todos os resultado da regex

const regex = /(Mister )\w+/g
const str = 'Mister Smith with Mister Galladon'

// Regex comum
const matches = new RegExp(regex).exec(str)
console.log(matches)
/*
[
  'Mister Smith',
  'Mister ',
  index: 0,
  input: 'Mister Smith with Mister Galladon',
  groups: undefined
] 
*/

// matchAll
const matchesAll = str.matchAll(regex)
console.log(Array.from(matchesAll))
/*
[
  [
    'Mister Smith',
    'Mister ',
    index: 0,
    input: 'Mister Smith with Mister Galladon',
    groups: undefined
  ],
  [
    'Mister Galladon',
    'Mister ',
    index: 18,
    input: 'Mister Smith with Mister Galladon',
    groups: undefined
  ]
]
*/